# Infrastructure Module - NPM Packages

This reusable infrastructure module supports [Google Workspace](https://workspace.google.com) site verification and Gmail routing, including DKIM.
