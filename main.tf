resource "aws_route53_record" "google_domain_main_verification_record" {
  zone_id = var.aws_zone_id
  name = ""
  type = "TXT"
  ttl = "3600"
  records = [
    var.domain_main_verification_key
  ]
}

resource "aws_route53_record" "google_domain_main_mail_record" {
  zone_id = var.aws_zone_id
  name = ""
  type = "MX"
  ttl = "3600"
  records = [
    "1 ASPMX.L.GOOGLE.COM.",
    "5 ALT1.ASPMX.L.GOOGLE.COM.",
    "5 ALT2.ASPMX.L.GOOGLE.COM.",
    "10 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT4.ASPMX.L.GOOGLE.COM."
  ]
}

resource "aws_route53_record" "google_domain_main_dkim_record" {
  zone_id = var.aws_zone_id
  name = "google._domainkey"
  type = "TXT"
  ttl = "3600"
  records = [
    join("\"\"", [
      substr(var.domain_main_dkim, 0, 255),
      substr(var.domain_main_dkim, 255, 255),
    ])
  ]
}
