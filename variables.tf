# AWS
variable "aws_zone_id" {
  type = string
  description = "The AWS Route53 zone ID"
}

# DNS
variable "domain_main" {
  type = string
  description = "The main domain name"
}

# Google
variable "domain_main_verification_key" {
  type = string
  description = "The Google site verification key for the main domain"
}

variable "domain_main_dkim" {
  type = string
  description = "The Gmail DKIM key for the main domain"
}
